module gitlab.com/pujo.j/dircache

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	gocloud.dev v0.17.0
)
