/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/pujo.j/dircache/server"
	"gocloud.dev/blob"
	_ "gocloud.dev/blob/azureblob"
	_ "gocloud.dev/blob/gcsblob"
	_ "gocloud.dev/blob/s3blob"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var TTL string

var bucketURL string
var pathPrefix string
var port int

var serverCmd = &cobra.Command{
	Use: "server",
	Args: func(cmd *cobra.Command, args []string) error {
		_, err := time.ParseDuration(TTL)
		if err != nil {
			return err
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		var bucket *blob.Bucket
		var err error
		ctx := context.Background()
		ttl, err := time.ParseDuration(TTL)
		if err != nil {
			log.WithError(err).WithField("TTL", TTL).Error("parsing TTL duration")
			os.Exit(1)
		}
		if bucketURL != "" {
			bucket, err = blob.OpenBucket(ctx, bucketURL)
			if err != nil {
				log.WithError(err).WithField("bucket", bucketURL).Error("initializing bucket")
				os.Exit(1)
			}
			log.WithField("bucketURL", bucketURL).WithField("bucket", bucket).Debug("using object store bucket")
		}
		s := server.NewServer(port)
		s.Bucket = bucket
		s.Prefix = pathPrefix
		s.TTL = ttl
		err = s.Start()
		if err != nil {
			log.WithError(err).Error("starting server")
			os.Exit(1)
		}
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
		select {
		case sig := <-sigs:
			if sig != nil {
				log.WithField("Signal", sig).Info("Received system signal, shutting down server")
				err = s.Stop()
				if err != nil {
					log.WithError(err).Error("shutting down server")
				}
				break
			}
		case <-s.CloseChannel:
			break
		}
	},
}

func init() {
	serverCmd.Flags().StringVarP(&TTL, "TTL", "t", "10m", "blob time to live")
	serverCmd.Flags().StringVarP(&bucketURL, "bucket", "b", "", "cloud storage bucket URL (none means local tmp filesystem)")
	serverCmd.Flags().StringVarP(&pathPrefix, "prefix", "p", "", "cloud storage path to use")
	serverCmd.Flags().IntVarP(&port, "port", "l", 443, "listen port to use (if your cluster has weird firewall rules... kof GKE private clusters kof...")
}
