/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
)

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.WithError(err).Error(err.Error())
	}
}

var debug bool

var rootCmd = &cobra.Command{
	Use:     "dircache",
	Short:   "simple directory caching and transfer for secure networks",
	Long:    "Exposes a simple http REST (POST and GET) abstraction over folder storage, permitting simple scripts to delegate object store API access and auth to the dircache gateway",
	Version: GetVersion(),
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		log.SetOutput(os.Stderr)
		if debug {
			log.SetLevel(log.DebugLevel)
		} else {
			log.SetLevel(log.InfoLevel)
		}
	},
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&debug, "verbose", "v", false, "Verbose Logging")
	rootCmd.AddCommand(serverCmd)
	rootCmd.AddCommand(getCmd)
	rootCmd.AddCommand(putCmd)
}
