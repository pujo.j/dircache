/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/pujo.j/dircache/client"
)

var dir string
var baseURL string

var putCmd = &cobra.Command{
	Use: "put",
	Run: func(cmd *cobra.Command, args []string) {
		remote := client.Remote{
			BaseURL: baseURL,
		}
		key, err := remote.Put(dir)
		if err != nil {
			log.WithError(err).Error("Pushing directory")
		} else {
			println(key)
		}
	},
}

func init() {
	putCmd.Flags().StringVarP(&dir, "dir", "d", "", "directory to push")
	putCmd.Flags().StringVarP(&baseURL, "base", "b", "http://dircache:8080", "dircache service base URL")
}
