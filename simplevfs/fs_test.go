/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simplevfs

import (
	"bytes"
	"os"
	"testing"
)

func TestSimpleDir(t *testing.T) {
	fs := NewVFS()
	err := AddLocalDir(fs, "./", nil)
	if err != nil {
		t.Error(err)
	}
	t.Logf("VFS: %v", fs)
}

func TestTarGz(t *testing.T) {
	fs := NewVFS()
	err := AddLocalDir(fs, "./", nil)
	if err != nil {
		t.Error(err)
	}
	out, err := os.Create("../tests/test.tar.gz")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_ = out.Close()
	}()
	err = TarGz(fs, out)
	if err != nil {
		t.Error(err)
	}
}

func TestUntarGz(t *testing.T) {
	fs := NewVFS()
	err := AddLocalDir(fs, "./", nil)
	if err != nil {
		t.Error(err)
	}
	buf := &bytes.Buffer{}
	err = TarGz(fs, buf)
	if err != nil {
		t.Error(err)
	}
	reader := bytes.NewReader(buf.Bytes())
	err = os.Mkdir("../tests/temp", 0700)
	if err != nil {
		t.Error(err)
	}
	vfs, err := UntarGzToDir("../tests/temp", reader)
	if err != nil {
		t.Error(err)
	}
	t.Logf("VFS: %v", vfs)
}
