/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simplevfs

import (
	"io"
	"time"
)

type VFS struct {
	Root *DirEntry
}

type Entry interface {
	Name() string
	Size() int64
	ModTime() time.Time
}

type ReadableEntry interface {
	Entry
	Reader() (io.ReadCloser, error)
}

type DirEntry struct {
	modTime  time.Time
	name     string
	parent   *DirEntry
	children []Entry
}

type FileEntry struct {
	size    int64
	modTime time.Time
	name    string
	parent  *DirEntry
}

func NewVFS() *VFS {
	return &VFS{Root: &DirEntry{
		modTime:  time.Now(),
		name:     "",
		parent:   nil,
		children: make([]Entry, 0),
	}}
}

func (d *DirEntry) Name() string {
	return d.name
}

func (d *DirEntry) Size() int64 {
	return 0
}

func (d *DirEntry) ModTime() time.Time {
	return d.modTime
}

func (f *FileEntry) Name() string {
	return f.name
}

func (f FileEntry) Size() int64 {
	return f.size
}

func (f FileEntry) ModTime() time.Time {
	return f.modTime
}

func (d *DirEntry) Children() []Entry {
	return d.children
}
