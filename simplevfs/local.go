/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simplevfs

import (
	"io"
	"io/ioutil"
	"os"
	path2 "path"
)

func AddLocalDir(vfs *VFS, path string, parentDir *DirEntry) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}
	var parent *DirEntry
	if parentDir == nil {
		parent = vfs.Root
	} else {
		parent = parentDir
	}
	for _, f := range files {
		if f.IsDir() {
			entry := DirEntry{
				modTime:  f.ModTime(),
				name:     f.Name(),
				parent:   parent,
				children: make([]Entry, 0),
			}
			parent.children = append(parent.children, &entry)
			err = AddLocalDir(vfs, path2.Join(path, f.Name()), &entry)
			if err != nil {
				return err
			}
		} else {
			entry := LocalFileEntry{
				FileEntry: FileEntry{
					modTime: f.ModTime(),
					name:    f.Name(),
					parent:  parent,
					size:    f.Size()},
				path: path2.Join(path, f.Name()),
			}
			parent.children = append(parent.children, &entry)
		}
	}
	return nil
}

type LocalFileEntry struct {
	FileEntry
	path string
}

func (l *LocalFileEntry) Reader() (io.ReadCloser, error) {
	return os.Open(l.path)
}
