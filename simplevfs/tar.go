/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package simplevfs

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
)

func TarGz(vfs *VFS, writer io.Writer) error {
	gw := gzip.NewWriter(writer)
	tw := tar.NewWriter(gw)
	defer func() {
		_ = tw.Close()
		_ = gw.Close()
	}()
	for _, c := range vfs.Root.Children() {
		err := addEntry(c, "", tw)
		if err != nil {
			return err
		}
	}
	return nil
}

func addEntry(entry Entry, basePath string, writer *tar.Writer) error {
	switch e := entry.(type) {
	case ReadableEntry:
		err := writer.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     path.Join(basePath, e.Name()),
			Size:     e.Size(),
			ModTime:  e.ModTime(),
		})
		if err != nil {
			return err
		}
		reader, err := e.Reader()
		if err != nil {
			return err
		}
		s, err := io.Copy(writer, reader)
		if err != nil {
			return err
		}
		if s != e.Size() {
			return errors.New("invalid file size")
		}
		return nil
	case *DirEntry:
		err := writer.WriteHeader(&tar.Header{
			Typeflag: tar.TypeDir,
			Name:     path.Join(basePath, e.Name()),
			Size:     e.Size(),
			ModTime:  e.ModTime(),
		})
		if err != nil {
			return err
		}
		for _, c := range e.Children() {
			err = addEntry(c, path.Join(basePath, e.Name()), writer)
			if err != nil {
				return err
			}
		}
		return nil
	default:
		return fmt.Errorf("invalid entry type for %v", entry)
	}
}

func UntarGzToDir(tmpDir string, reader io.Reader) (*VFS, error) {
	gr, err := gzip.NewReader(reader)
	if err != nil {
		return nil, err
	}
	tr := tar.NewReader(gr)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break // End of archive
		}
		if err != nil {
			return nil, err
		}
		filename := path.Join(tmpDir, hdr.Name)
		switch hdr.Typeflag {
		case tar.TypeDir:
			err = os.Mkdir(filename, 0700)
			if err != nil {
				return nil, err
			}
		case tar.TypeReg:
			fd, err := os.Create(filename)
			if err != nil {
				return nil, err
			}
			_, err = io.Copy(fd, tr)
			if err != nil {
				return nil, err
			}
			err = fd.Close()
			if err != nil {
				return nil, err
			}
			err = os.Chtimes(filename, hdr.ModTime, hdr.ModTime)
			if err != nil {
				return nil, err
			}
		}
	}
	res := NewVFS()
	err = AddLocalDir(res, tmpDir, res.Root)
	return res, err
}
