# Dircache

A simple cloud storage gateway for small directories in k8s clusters

## Rationale

The primary need is for krun, to inject script data into the init container.

It generally can be used to provide some dynamic context into containers for batch use, when context is too heavy to be passed as env variables.

 



 
