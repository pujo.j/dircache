/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package client

import (
	"errors"
	"gitlab.com/pujo.j/dircache/simplevfs"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type Remote struct {
	BaseURL string
}

func (r *Remote) Get(key string, outputDir string) error {
	res, err := http.Get(r.BaseURL + "/" + key)
	if err != nil {
		return err
	}
	defer func() { _ = res.Body.Close() }()
	if res.StatusCode != http.StatusOK {
		return errors.New("http " + strconv.Itoa(res.StatusCode) + " : " + res.Status)
	}
	_, err = simplevfs.UntarGzToDir(outputDir, res.Body)
	return err
}

func (r *Remote) Put(inputDir string) (string, error) {
	fs := simplevfs.NewVFS()
	err := simplevfs.AddLocalDir(fs, inputDir, fs.Root)
	if err != nil {
		return "", err
	}
	pipeReader, pipeWriter := io.Pipe()
	doneChan := make(chan error)
	go func() {
		err := simplevfs.TarGz(fs, pipeWriter)
		if err != nil {
			doneChan <- err
		} else {
			close(doneChan)
		}
		_ = pipeWriter.Close()
	}()
	res, err := http.Post(r.BaseURL, "application/tar+gzip", pipeReader)
	if err != nil {
		return "", err
	}
	if res.StatusCode != 201 {
		return "", errors.New("http " + strconv.Itoa(res.StatusCode) + " : " + res.Status)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	key := strings.Trim(string(body), "\n\r ")
	err = <-doneChan
	return key, err
}
