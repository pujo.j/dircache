/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package server

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"errors"
	log "github.com/sirupsen/logrus"
	"gocloud.dev/blob"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Server struct {
	Bucket       *blob.Bucket
	Prefix       string
	TTL          time.Duration
	HttpServer   *http.Server
	CloseChannel chan struct{}
	stateLock    sync.Mutex
	started      bool
}

func NewServer(port int) *Server {
	res := &Server{
		HttpServer: &http.Server{
			Addr: ":" + strconv.Itoa(port),
		},
		CloseChannel: make(chan struct{}),
	}
	res.HttpServer.Handler = res
	return res
}

func (s *Server) Start() error {
	s.stateLock.Lock()
	defer s.stateLock.Unlock()
	if s.started {
		return errors.New("server already started")
	}
	err := os.Mkdir(tempDir, 0700)
	if err != nil {
		return err
	}
	go func() {
		log.
			WithField("bucket", s.Bucket).
			WithField("prefix", s.Prefix).
			WithField("TTL", s.TTL).
			Info("Starting dircache gateway server")
		err := s.HttpServer.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.WithError(err).Fatal("starting http server")
		}
	}()
	go s.gc()
	s.started = true
	return nil
}

func (s *Server) gc() {
	// Garbage collecting goroutine
	log.Debug("Starting garbage collection goroutine")
	ctx := context.Background()
	t := time.NewTicker(60 * time.Second)
	for {
		select {
		case <-s.CloseChannel:
			return
		case <-t.C:
			{
				now := time.Now()
				if s.Bucket != nil {
					blobs := s.Bucket.List(&blob.ListOptions{
						Prefix:    s.Prefix,
						Delimiter: "/",
					})
					b, err := blobs.Next(ctx)
					if err != nil {
						log.WithError(err).Error(err.Error())
						break
					}
					for b != nil {
						if now.Sub(b.ModTime) > s.TTL {
							log.Debugf("deleting blob %s", b.Key)
							err = s.Bucket.Delete(ctx, b.Key)
							if err != nil {
								log.WithError(err).WithField("file", b.Key).Error("deleting blob")
							}
						}
						b, err = blobs.Next(ctx)
						if err != nil {
							log.WithError(err).Error(err.Error())
							break
						}
					}
				} else {
					files, err := ioutil.ReadDir(tempDir)
					if err != nil {
						log.WithError(err).Error(err.Error())
						break
					}
					for _, file := range files {
						if now.Sub(file.ModTime()) > s.TTL {
							filePath := path.Join(tempDir, file.Name())
							log.Debugf("deleting blob %s", filePath)
							err = os.Remove(filePath)
							if err != nil {
								log.WithError(err).WithField("file", filePath).Error("deleting file")
							}
						}
					}
				}
			}
		}
	}
}

func (s *Server) Stop() error {
	s.stateLock.Lock()
	defer s.stateLock.Unlock()
	if !s.started {
		log.Error("trying to stop a non running server")
		return nil
	}
	defer func() {
		_ = os.RemoveAll(tempDir)
	}()
	close(s.CloseChannel)
	err := s.HttpServer.Close()
	if err == nil {
		s.started = false
	}
	return err
}

func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	defer func() {
		_ = req.Body.Close()
	}()
	log.WithField("request", req).Debug("processing http request")
	switch req.Method {
	case "POST":
		// Generate uid
		uidBytes := make([]byte, 16)
		_, _ = rand.Read(uidBytes)
		uidString := hex.EncodeToString(uidBytes)
		log.WithField("uid", uidString).Debug("generated blob UID")
		if s.Bucket != nil {
			writer, err := s.Bucket.NewWriter(req.Context(), s.Prefix+"/"+uidString, &blob.WriterOptions{
				ContentType: "application/tar+gzip",
			})
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
			defer func() {
				_ = writer.Close()
			}()
			_, err = io.Copy(writer, req.Body)
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
			log.Debugf("added blob at Key:  %s", uidString)
			res.Header().Set("Location", uidString)
			res.WriteHeader(201)
			_, _ = res.Write([]byte(uidString))
		} else {
			file, err := os.Create(path.Join(tempDir, uidString))
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
			defer func() {
				_ = file.Close()
			}()
			_, err = io.Copy(file, req.Body)
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
			log.Debugf("added blob at Key:  %s", uidString)
			res.Header().Set("Location", uidString)
			res.WriteHeader(201)
			_, _ = res.Write([]byte(uidString))
		}
	case "GET":
		uidString := strings.Trim(req.URL.Path, "/")
		_, err := hex.DecodeString(uidString)
		if err != nil {
			res.WriteHeader(404)
			return
		}
		if s.Bucket != nil {
			stat, err := s.Bucket.Attributes(req.Context(), s.Prefix+"/"+uidString)
			if err != nil {
				res.WriteHeader(404)
				return
			}
			reader, err := s.Bucket.NewReader(req.Context(), s.Prefix+"/"+uidString, &blob.ReaderOptions{})
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
			defer func() {
				_ = reader.Close()
			}()
			log.Debugf("getting blob at Key: %s", uidString)
			res.Header().Set("Content-Type", "application/tar+gzip")
			res.Header().Set("Content-Length", strconv.FormatInt(stat.Size, 10))
			res.WriteHeader(200)
			_, err = io.Copy(res, reader)
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
		} else {
			file, err := os.Open(path.Join(tempDir, uidString))
			if err != nil && err == os.ErrNotExist {
				res.WriteHeader(404)
				return
			}
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
			stat, err := file.Stat()
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
			defer func() {
				_ = file.Close()
			}()
			log.Debugf("getting blob at Key:  %s", uidString)
			res.Header().Set("Content-Type", "application/tar+gzip")
			res.Header().Set("Content-Length", strconv.FormatInt(stat.Size(), 10))
			res.WriteHeader(200)
			_, err = io.Copy(res, file)
			if err != nil {
				log.WithError(err).Error(err.Error())
				res.WriteHeader(500)
				return
			}
		}
	default:
		res.WriteHeader(405)
		return
	}
}

var tempDir = path.Join(os.TempDir(), "dc_"+strconv.Itoa(os.Getpid()))
